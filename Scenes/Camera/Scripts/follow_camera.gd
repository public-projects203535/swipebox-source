class_name FollowCamera
extends Camera3D

signal camera_reset

const START_FOV : float = 80
const END_FOV : float = 50


@export_category("Camera Shake")
@export var noise : FastNoiseLite
@export var noise_speed :=  50.0
@export var trauma_reduction_rate := 1.0
@export var max_x := 10.0
@export var max_y := 10.0
@export var max_z := 5.0
@export_category("Camera Movement")
@export var movement_speed : float = 2.5
@export var rotation_speed : float = 3.0
@export var fov_speed : float = 3.0
@export var seperation_distance : float = 1
@export var follow_rotation : float = -20

var _start_position : Vector3
var target_position : Vector3
var target_rotation : float

var _current_fov_speed : float = 3.0
var _current_rotation_speed : float = 3.0

var _should_update_position := true

var _trauma := 0.0
var _initial_rotation := Vector3(0,0,0)
var _time : float = 0

var _is_shaking := false

func _ready():
	_current_fov_speed = fov_speed
	_current_rotation_speed = rotation_speed
	_initial_rotation = rotation_degrees
	_start_position = Vector3(transform.origin.x, 0.4, transform.origin.z)
	target_position = _start_position
	#print(start_position)
	GameState.game_started.connect(Callable(_on_game_start))
	GameState.game_ended.connect(Callable(_on_game_end))


func _process(delta):
	if _is_shaking:
		_trauma = max(_trauma - delta * trauma_reduction_rate, 0.0)
		_time += delta	
		rotation_degrees.x = _initial_rotation.x + max_x * _get_shake_intensity() * _get_noise_from_seed(0)
		rotation_degrees.y = _initial_rotation.y + max_y * _get_shake_intensity() * _get_noise_from_seed(1)
		rotation_degrees.z = _initial_rotation.z + max_z * _get_shake_intensity() * _get_noise_from_seed(2)
		if _trauma <= 0:
			_is_shaking = false

	update_camera_position(delta)
	update_camera_rotation(delta)
	update_camera_fov(delta)
	


#<---Public functions--->

func update_camera_position(delta : float) -> void:
	# If camera returned to the starting area
	if _should_update_position and _nearly_equal(target_position.y + seperation_distance, global_position.y, 0.2):
		_should_update_position = false
		camera_reset.emit()
		
	var cam_y : float = transform.origin.y
	var new_y := lerpf(cam_y, target_position.y + seperation_distance, movement_speed * delta)
	transform.origin = Vector3(transform.origin.x, new_y, transform.origin.z)
	

func update_camera_rotation(delta : float) -> void:
	if GameState.is_game_started() and target_rotation != follow_rotation:
		target_rotation = follow_rotation
	elif !GameState.is_game_started() and target_rotation != 0:
		target_rotation = 0

	var new_x := lerpf(rotation_degrees.x, target_rotation , _current_rotation_speed * delta)
	rotation_degrees = Vector3(new_x, rotation_degrees.y, rotation_degrees.z)


func update_camera_fov(delta : float) -> void:
	var target_fov = END_FOV if GameState.is_game_started() else START_FOV
	var new_fov := lerpf(fov, target_fov, _current_fov_speed * delta)
	fov = new_fov


func shake() -> void:
	_initial_rotation = rotation_degrees
	_time = 0
	_trauma = 0
	_is_shaking = true
	add_trauma(1)


func add_trauma(trauma_amount : float) -> void:
	_trauma = clamp(_trauma + trauma_amount, 0.0, 1.0)
	


#<---Private functions--->

func _nearly_equal( f1 : float, f2 :float, epsilon : float ) -> bool:
	var diff = f1 - f2
	return abs( diff ) < epsilon


func _get_shake_intensity() -> float:
	return _trauma * _trauma


func _get_noise_from_seed(_seed : int) -> float:
	noise.seed = _seed
	return noise.get_noise_1d(_time * noise_speed)



#<---Signal functions---> 

func _on_game_start() -> void:
	_current_rotation_speed = rotation_speed
	_current_fov_speed = fov_speed
	_should_update_position = false


func _on_game_end() -> void:
	_should_update_position = true
	target_position = _start_position
	_current_rotation_speed = 1.5
	_current_fov_speed = 1.5


func _on_timer_timeout() -> void:
	shake()
