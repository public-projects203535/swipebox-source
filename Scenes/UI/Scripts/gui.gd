extends CanvasLayer

@onready var AnimPlayer : AnimationPlayer = $AnimationPlayer
@onready var MainMenu : Control = $MainMenu
@onready var Settings : Control = $Settings
@onready var Tutorial : Control = $Tutorial
@onready var Difficulty : Control = $DifficultyInfo
@onready var Credits : Control = $Credits

func _ready():
	MainMenu.modulate = Color(1,1,1,0)
	GameState.game_started.connect(Callable(_on_game_start))


#<---Public functions--->


func show_main_menu() -> void:
	_show_menu(MainMenu)
	AnimPlayer.play("FadeIn")


#<---Private functions--->

func _show_menu(menu : Control,  set_modulate := false) -> void:
	menu.visible = true
	menu.mouse_filter = Control.MOUSE_FILTER_STOP
	if set_modulate:
		menu.modulate = Color(1,1,1,1)


func _hide_menu(menu : Control, set_modulate := false) -> void:
	menu.visible = false
	menu.mouse_filter = Control.MOUSE_FILTER_IGNORE
	if set_modulate:
		menu.modulate = Color(1,1,1,0)



#<---Signal functions---> 

func _on_game_start() -> void:
	_hide_menu(MainMenu, true)


func _on_main_menu_settings_pressed() -> void:
	_hide_menu(MainMenu)
	_show_menu(Settings)
	GameState.is_in_menu = true


func _on_settings_back_pressed() -> void:
		_hide_menu(Settings)
		_show_menu(MainMenu)
		GameState.is_in_menu = false


func _on_tutorial_back_pressed() -> void:
	_hide_menu(Tutorial)
	_show_menu(Settings)


func _on_settings_tutorial_pressed() -> void:
	_hide_menu(Settings)
	_show_menu(Tutorial)


func _on_settings_difficulty_pressed() -> void:
	_hide_menu(Settings)
	_show_menu(Difficulty)


func _on_difficulty_info_back_pressed() -> void:
		_hide_menu(Difficulty)
		_show_menu(Settings)


func _on_credits_back_pressed() -> void:
		_hide_menu(Credits)
		_show_menu(Settings)


func _on_settings_credits_pressed() -> void:
	_hide_menu(Settings)
	_show_menu(Credits)
