extends Control

signal settings_pressed

@onready var option_button : OptionButton = $HBoxContainer/DifficultyContainer/DifficultySelection
@onready var popup : PopupMenu = option_button.get_popup()

func _ready():
	# Connect the about_to_show signal to adjust the position
	popup.about_to_popup.connect(Callable(_on_popup_about_to_show))

func _on_popup_about_to_show():
	var popup_size = popup.size
	var button_position = option_button.global_position

	# Adjust the popup's position to make it appear directly above the OptionButton
	popup.position = Vector2(button_position.x, button_position.y - popup_size.y)



func _on_settings_button_pressed() -> void:
	settings_pressed.emit()


func _on_difficulty_selection_item_selected(index: int) -> void:
	GameState.switch_mode(index)
