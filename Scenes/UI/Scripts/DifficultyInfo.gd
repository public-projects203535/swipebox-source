extends Control

signal back_pressed

@onready var Pages := $Control2/Pages

var _current_page := 1

#<---Private functions--->

func _reset() -> void:
	_current_page = 1
	$Control2/BackPageButton.visible = false
	$Control2/NextPageButton.visible = true
	for page in Pages.get_children():
		page.visible = true

#<---Signal functions---> 


func _on_next_page_button_pressed() -> void:
	$Control2/BackPageButton.visible = true
	var index = Pages.get_child_count() - _current_page
	Pages.get_child(index).visible = false
	_current_page += 1
	if _current_page == Pages.get_child_count():
		$Control2/NextPageButton.visible = false


func _on_back_page_button_pressed() -> void:
	_current_page -= 1
	var index = Pages.get_child_count() - _current_page
	Pages.get_child(index).visible = true
	if _current_page <= Pages.get_child_count():
		$Control2/NextPageButton.visible = true
	if _current_page == 1:
		$Control2/BackPageButton.visible = false


func _on_back_button_pressed() -> void:
	_reset()
	back_pressed.emit()
