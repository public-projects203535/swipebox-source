extends Control

signal back_pressed

func _on_back_page_button_pressed() -> void:
	$Control2/Page1.visible = true
	$Control2/NextPageButton.visible = true
	$Control2/BackPageButton.visible = false


func _on_next_page_button_pressed() -> void:
	$Control2/Page1.visible = false
	$Control2/NextPageButton.visible = false
	$Control2/BackPageButton.visible = true


func _on_back_button_pressed() -> void:
	_on_back_page_button_pressed()
	back_pressed.emit()
