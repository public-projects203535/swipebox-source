class_name ArrowPrompts
extends Control

signal arrow_swiped(direction : int)
signal arrow_miss(was_trap : bool)

const BASE_ANIM_NAME : String = "BaseAnimation"
const LEFT_ANIM_NAME : String = "LeftToRight"
const RIGHT_ANIM_NAME : String = "RightToLeft"
const TOP_ANIM_NAME : String = "TopToDown"
const START_ANIM_NAME : String = "Start"

var normal_arrow = preload("res://Assets/Textures/arrow_thin.png")
var red_arrow = preload("res://Assets/Textures/arrow_thin_red.png")

@export var prompt_speed : float = 1.7

@onready var AnimPlayer := $AnimationPlayer
@onready var TopArrow := $TopContainer/TopArrow
@onready var LeftArrow := $LeftContainer/LeftArrow
@onready var RightArrow := $RightContainer/RightArrow
@onready var Finger := $FingerContainer/Finger
@onready var FingerTimer := $FingerTimer
@onready var IdleTimer := $IdleTimer

enum ArrowType{LEFT, RIGHT, TOP, START}

var viewport_width := 0.0

var target_prompt_speed := 1.0
var current_prompt_speed := 0.5
var slowdown_tick := 1.0 
var is_slowed := false

var is_animating := false

var _current_arrow : ArrowType
var _is_interpolating_speed := false
var _is_trap := false

var _finger_tween : Tween
var _finger_start_pos := Vector2.ZERO

func _ready():
	target_prompt_speed = prompt_speed
	AnimPlayer.speed_scale = current_prompt_speed
	_setup_animations()
	GameState.game_started.connect(Callable(_on_game_started))
	GameState.game_ended.connect(Callable(_on_game_ended))
	_finger_start_pos = Finger.global_position
	start_idle_timer()
	

func _process(delta):
	if !is_animating:
		return
	if _is_interpolating_speed:
		current_prompt_speed += slowdown_tick * delta
		AnimPlayer.speed_scale = current_prompt_speed
		if current_prompt_speed >= target_prompt_speed:
			current_prompt_speed = target_prompt_speed
			AnimPlayer.speed_scale = current_prompt_speed
			_is_interpolating_speed = false
			if is_slowed:
				is_slowed = false
				slowdown_tick = 1.0


	
#<---Public functions--->

func animate_arrow(arrow : ArrowType, is_trap := false) -> void:
	_current_arrow = arrow
	_is_trap = is_trap
	if _is_trap:
		_update_arrow_texture(is_trap)
	match arrow:
		ArrowType.LEFT:
			AnimPlayer.play("LeftToRight", -1)
		ArrowType.RIGHT:
			AnimPlayer.play("RightToLeft", -1)
		ArrowType.TOP:
			AnimPlayer.play("TopToDown", -1)
	is_animating = true


func animate_finger() -> void:
	Finger.global_position = _finger_start_pos
	_finger_tween = get_tree().create_tween()
	await _finger_tween.tween_property(Finger, "modulate", Color(1,1,1,1), 0.25).finished
	_finger_tween = get_tree().create_tween().set_trans(Tween.TRANS_SINE)
	await _finger_tween.tween_property(Finger, "global_position", Vector2(Finger.global_position.x, Finger.global_position.y + 360), 2).finished
	await get_tree().create_timer(0.10).timeout
	_finger_tween = get_tree().create_tween()
	await _finger_tween.tween_property(Finger, "modulate", Color(1,1,1,0), 0.60).finished
	if !GameState.is_game_started():
		FingerTimer.start()


func show_start_arrow() -> void:
	AnimPlayer.stop()
	AnimPlayer.play("Start", -1, 1.0)
	_current_arrow = ArrowType.START
	is_animating = true


func stop_animations() -> void:
	AnimPlayer.stop()
	stop_finger_anim()


func start_idle_timer() -> void:
	IdleTimer.start()


func set_prompt_speed(_prompt_speed : float) -> void:
	current_prompt_speed = clamp(_prompt_speed, 0.5, target_prompt_speed)
	AnimPlayer.speed_scale = current_prompt_speed
	_is_interpolating_speed = true


func override_prompt_speed(_prompt_speed : float) -> void:
	current_prompt_speed = _prompt_speed
	AnimPlayer.speed_scale = current_prompt_speed
	

func set_target_prompt_speed(_prompt_speed : float) -> void:
	target_prompt_speed = _prompt_speed
	_is_interpolating_speed = true


func stop_finger_anim() -> void:
	IdleTimer.stop()
	FingerTimer.stop()
	if is_instance_valid(_finger_tween):
		_finger_tween.stop()
	Finger.modulate = Color(1,1,1,0)



#<---Private functions--->
	
func _setup_animations() -> void:
	var animation : Animation = AnimPlayer.get_animation(BASE_ANIM_NAME)
	var duplicate_anim : Animation = animation.duplicate()
	duplicate_anim.resource_name = LEFT_ANIM_NAME
	
	# Create animation for each arrow based off of the base animation
	AnimPlayer.get_animation_library("").add_animation(LEFT_ANIM_NAME, animation.duplicate())
	AnimPlayer.get_animation_library("").add_animation(RIGHT_ANIM_NAME, animation.duplicate())
	AnimPlayer.get_animation_library("").add_animation(TOP_ANIM_NAME, animation.duplicate())
	AnimPlayer.get_animation_library("").add_animation(START_ANIM_NAME, animation.duplicate())
	_set_arrow_animation(LEFT_ANIM_NAME,"LeftContainer/LeftArrow", LeftArrow.global_position, Vector2(LeftArrow.global_position.x + 360, LeftArrow.global_position.y))
	_set_arrow_animation(RIGHT_ANIM_NAME,"RightContainer/RightArrow", RightArrow.global_position, Vector2(RightArrow.global_position.x - 360, RightArrow.global_position.y))
	_set_arrow_animation(TOP_ANIM_NAME, "TopContainer/TopArrow", TopArrow.global_position, Vector2(TopArrow.global_position.x, TopArrow.global_position.y + 360) )
	_set_arrow_animation(START_ANIM_NAME, "TopContainer/TopArrow", TopArrow.global_position, Vector2(TopArrow.global_position.x, TopArrow.global_position.y + 360) )
	

func _set_arrow_animation(animation_name : String, track_path : String, start_position : Vector2, end_position : Vector2) -> void:
	var animation : Animation = AnimPlayer.get_animation(animation_name)
	# Change the modulate property path for all arrows except left which is already set
	if animation_name != LEFT_ANIM_NAME:
		for i in range(animation.get_track_count()):
			var concat_name = animation.track_get_path(i).get_concatenated_names() + ":" + animation.track_get_path(i).get_concatenated_subnames()
			if concat_name == "LeftContainer/LeftArrow:modulate":
				animation.track_set_path(i, "%s:modulate" % track_path)
			
	# Add movement track
	var track_idx := animation.add_track(Animation.TYPE_VALUE)
	animation.track_set_path(track_idx, "%s:global_position" % track_path)
	animation.track_insert_key(track_idx, 0.0, start_position)
	animation.track_insert_key(track_idx, 1.20, end_position)
	animation.track_set_interpolation_type(track_idx, Animation.INTERPOLATION_CUBIC)


func _handle_input(input_type : int) -> void:
	if not is_animating:
		return
	if input_type == InputManager.InputType.MISS:
		_miss(false, true)
		return
	
	var is_input_correct := false
	if input_type == InputManager.InputType.DOWN:
		is_input_correct = _input_correct(0)
	elif input_type == InputManager.InputType.LEFT:
		is_input_correct = _input_correct(1)
	elif input_type == InputManager.InputType.RIGHT:
		is_input_correct = _input_correct(2)
	else:
		return
	
	if is_input_correct:
		if _is_trap:
			_miss()
		else:
			_on_arrow_swiped()
	else:
		if _is_trap:
			_on_arrow_swiped()
		else:
			_miss()
		

func _on_arrow_swiped() -> void:
	is_animating = false
	_update_arrow_texture(false)
	AnimPlayer.stop()
	arrow_swiped.emit(_current_arrow)


func _miss(timeout := false, debug_force_miss := false) -> void:
	# If didn't swipe a trap
	if _is_trap and timeout:
		_on_arrow_swiped()
		return
		
	if _current_arrow == ArrowType.START:
		if timeout:
			show_start_arrow()
		return
	
	is_animating = false
	
	var was_trap = _is_trap
	if _is_trap:
		_is_trap = false
		_update_arrow_texture(_is_trap)

	AnimPlayer.stop()
	if GameState.debug_auto_prompt and !debug_force_miss:
		arrow_swiped.emit(_current_arrow)
		return
	arrow_miss.emit(was_trap)


func _update_arrow_texture(is_trap : bool) -> void:
	match _current_arrow:
		ArrowType.LEFT:
			if is_trap:
				LeftArrow.texture = red_arrow
			else:
				LeftArrow.texture = normal_arrow
		ArrowType.RIGHT:
			if is_trap:
				RightArrow.texture = red_arrow
			else:
				RightArrow.texture = normal_arrow
		ArrowType.TOP:
			if is_trap:
				TopArrow.texture = red_arrow
			else:
				TopArrow.texture = normal_arrow


func _get_screen_center() -> Vector2:
	return Vector2(get_viewport().size.x / 2, get_viewport().size.x / 2)
	

func _input_correct(input : int) -> bool:
	return ((input == 0 and (_current_arrow == ArrowType.TOP or _current_arrow == ArrowType.START)) 
			or (input == 1 and _current_arrow == ArrowType.RIGHT)  
			or (input == 2 and _current_arrow == ArrowType.LEFT))



#<---Signal functions---> 


func _on_game_started() -> void:
	stop_finger_anim()
	
	
func _on_game_ended() -> void:
	IdleTimer.start()


func _on_finger_timer_timeout() -> void:
	animate_finger()


func _on_idle_timer_timeout() -> void:
	if !GameState._game_started:
		FingerTimer.start()


func _on_input_manager_swipe(input_type) -> void:
	_handle_input(input_type)
