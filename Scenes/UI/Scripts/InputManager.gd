class_name InputManager
extends Node

signal swipe(input_type : InputType)

enum InputType{NONE, DOWN, LEFT, RIGHT, MISS}

@export var drag_sensitivity : float = 30

var _can_swipe = true
var _first_touch = true
var _mouse_down = false

var _touch_start := Vector2.ZERO


func _unhandled_input(event: InputEvent) -> void:
	if GameState.is_in_menu:
		return
		
	var _input_type : InputType = InputType.NONE
	
	# Handle screen touch
	if event.is_action_pressed("touch_start"):
		_mouse_down = true
	elif event.is_action_released("touch_start"):
		_mouse_down = false
		_can_swipe = true
		_first_touch = true
	if event is InputEventScreenTouch:
		# Reset if finger released
		if !event.pressed:
			_can_swipe = true
			_first_touch = true
		
	# Drag input
	if event is InputEventScreenDrag or (event is InputEventMouseMotion and _mouse_down):
		if _first_touch:
			_touch_start = event.position
			_first_touch = false
			return
		if !_can_swipe:
			return
		
		var touch_end : Vector2 = event.position
		var touch_diff = _touch_start - touch_end
		if touch_diff.length() <= drag_sensitivity:
			return
		
		_can_swipe = false
		# Is swiping horizontally more than vertically
		if absf(touch_diff.x) > absf(touch_diff.y):
			if touch_diff.x > 0:
				_input_type = InputType.LEFT
			else:
				_input_type = InputType.RIGHT
		elif touch_diff.y < 0:
			_input_type = InputType.DOWN

	# Keyboard input
	if event.is_action_pressed("miss"):
		_input_type = InputType.MISS
	elif event.is_action_pressed("down"):
		_input_type = InputType.DOWN
	elif event.is_action_pressed("left"):
		_input_type = InputType.LEFT
	elif event.is_action_pressed("right"):
		_input_type = InputType.RIGHT

	if _input_type == InputType.NONE:
		return
	swipe.emit(_input_type)	
	
	
