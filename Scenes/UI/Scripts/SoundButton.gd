extends TextureButton

var sound_on := preload("res://Assets/Textures/UI/audioOn.png")
var sound_off := preload("res://Assets/Textures/UI/audioOff.png")

func _pressed() -> void:
	GameState.sound_enabled = !GameState.sound_enabled
	if !GameState.sound_enabled:
		texture_normal = sound_off
	else:
		texture_normal = sound_on

#<---Public functions--->



#<---Private functions--->



#<---Signal functions---> 



