extends Control

signal back_pressed
signal tutorial_pressed
signal difficulty_pressed
signal credits_pressed


func _on_back_button_pressed() -> void:
	back_pressed.emit()


func _on_tutorial_button_pressed() -> void:
	tutorial_pressed.emit()


func _on_difficulty_button_pressed() -> void:
	difficulty_pressed.emit()


func _on_credits_button_pressed() -> void:
	credits_pressed.emit()
