class_name BoxPool
extends Node

@export var box_pool_size : int = 40
@export var tower_pool_size : int = 40
@export var max_boxes : int = 36

# Note: Better approach would be to remove pooled boxes and towers from scene tree and use an array instead
		# But performance difference is insignificant for this game so doesnt warrant refactor imo
@onready var PlacedBoxes := $PlacedBoxes
@onready var PooledBoxes := $PooledBoxes
@onready var PlacedTowers := $PlacedTowers
@onready var PooledTowers := $PooledTowers

var box_res := preload("res://Scenes/Props/box.tscn")
var tower_res := preload("res://Scenes/Props/tower.tscn")
var _animate_next := false


func _ready():
	_init_boxes()


#<---Public functions--->
	
func place_box(pos : Vector3, direction : int, text : String) -> void:
	# Replaces boxes with a tower if reached max box limit
	if PlacedBoxes.get_child_count() >= max_boxes:
		var first_pos : Vector3 = PlacedBoxes.get_child(0).global_position
		remove_boxes(max_boxes / 2, false, false)
		place_tower(first_pos)
		
	var box : Box
	if PooledBoxes.get_children().is_empty():
		box = _add_new_box_to_pool()
	else:
		box = PooledBoxes.get_child(0)
	box.reparent(PlacedBoxes)
	box.global_transform.origin = pos + _get_box_placement_pos(direction)
	box.set_text(text)
	box.move(pos)
	if _animate_next:
		box.animate()
		_animate_next = false
	

# Returns the amount of boxes removed
func remove_boxes(amount : int, remove_from_top := true, animate := true) -> int:
	if PlacedBoxes.get_children().is_empty():
		return 0
	for i in range(amount):
		if PlacedBoxes.get_children().is_empty():
			return i
		
		var box : Box = PlacedBoxes.get_children().back() if remove_from_top else PlacedBoxes.get_children().front()
		if animate:
			box.remove(Vector3(0,0,10))
		else:
			box.global_transform.origin = Vector3(0,0,10)
		box.reparent(PooledBoxes)
	return amount


func place_tower(pos : Vector3) -> void:
	var tower : Node3D
	if PooledTowers.get_children().is_empty():
		tower = _add_new_tower_to_pool()
	else:
		tower = PooledTowers.get_child(0)
	tower.reparent(PlacedTowers)
	tower.global_transform.origin = pos


func reset() -> void:
	if !PlacedTowers.get_children().is_empty():
		for i in range(PlacedTowers.get_child_count()):
			if PlacedTowers.get_children().is_empty():
				break
			var tower : Node3D = PlacedTowers.get_children().back()
			tower.global_transform.origin = Vector3(0,0,10)
			tower.reparent(PooledTowers)
		
	if PlacedBoxes.get_children().is_empty():
		return
	for i in range(PlacedBoxes.get_child_count()):
		if PlacedBoxes.get_children().is_empty():
			return
		var box : Box = PlacedBoxes.get_children().back()
		box.global_transform.origin = Vector3(0,0,10)
		box.reparent(PooledBoxes)


func animate_top_box() -> void:
	_animate_next = true



#<---Private functions--->

func _init_boxes() -> void:
	for i in range(box_pool_size):
		_add_new_box_to_pool()
	for i in range(tower_pool_size):
		_add_new_tower_to_pool()


func _add_new_box_to_pool() -> Box:
	var box : Box = box_res.instantiate()
	PooledBoxes.add_child(box)
	box.global_transform.origin = Vector3(0,0,10)
	return box


func _add_new_tower_to_pool() -> Node3D:
	var tower := tower_res.instantiate()
	PooledTowers.add_child(tower)
	tower.global_transform.origin = Vector3(0,0,10)
	return tower


func _get_box_placement_pos(direction : int) -> Vector3:
	match direction:
		0:
			return Vector3(0, 0, 3)
		1:
			return Vector3(0, 0, -3)
		2:
			return Vector3(3, 0, 0)
	return Vector3(0, 0, 3)

