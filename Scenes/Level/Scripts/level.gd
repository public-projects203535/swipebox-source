extends Node3D

signal begin

var box_res := preload("res://Scenes/Props/box.tscn")

enum ArrowType{LEFT, RIGHT, TOP}
const box_separation : float = 0.255

@export var game_mode : GameMode
@export var starting_prompt_speed : float = 0.33

@onready var Arrows : ArrowPrompts = $CanvasLayer/ArrowPrompts
@onready var Table := $Table
@onready var StartPoint := $Table/StartPoint
@onready var FollowCam := $FollowCamera
@onready var Box_Pool : BoxPool = $BoxPool
@onready var GameStartAnimation := $GameStartAnimation
@onready var BananaSpawner := $BananaSpawner

const _start_prompt_speed_increase := 0.28
var _current_starting_prompt_speed := 0.33
var _start_prompt_step : int = 0

var _rng = RandomNumberGenerator.new()

var _next_point : Vector3
var _next_score_step : int = 0
var _previous_score_step : int = 0

func _ready():
	_rng.randomize()
	_current_starting_prompt_speed = starting_prompt_speed
	_next_point = StartPoint.global_transform.origin
	GameState.menu_entered.connect(Callable(pause_game))
	GameState.menu_exited.connect(Callable(resume_game))
	GameState.gamemode_changed.connect(Callable(change_gamemode))
	GameState.high_score.connect(Callable(_on_high_score))



#<---Public functions--->

func begin_playing() -> void:
	if GameState.total_boxes == 0:
		await GameStartAnimation.play()
	elif GameState.total_boxes == 1:
		await GameStartAnimation.play_second()
	begin.emit()
	# Wait until start and menus finish before beginning
	await get_tree().create_timer(0.75).timeout
	_animate_start_arrow()


func change_gamemode(mode : GameMode) -> void:
	game_mode = mode


func resume_game() -> void:
	_animate_start_arrow()
	Arrows.start_idle_timer()


func pause_game() -> void:
	Arrows.stop_animations()


func start_game() -> void:
	GameStartAnimation.reset()
	_next_point = StartPoint.global_transform.origin
	Box_Pool.reset()
	
	_current_starting_prompt_speed = starting_prompt_speed
	_start_prompt_step = 0
	_previous_score_step = 0
	_next_score_step = game_mode.score_step_amount
	GameState.start_game(game_mode.max_tries)
	

func end_game() -> void:
	if GameState.total_boxes >= 2:
		GameStartAnimation.play_third()
	
	GameState.end_game()
	Arrows.target_prompt_speed = 0.5
	Arrows.override_prompt_speed(0.5)


func set_next_point(add_amount := Vector3(0, box_separation, 0)) -> void:
	_next_point = _next_point + add_amount
	FollowCam.target_position = _next_point



#<---Private functions--->

func _animate_random_arrow() -> void:
	_calculate_prompt_speed()
	var arrow : int = _rng.randi_range(0, 2)
	var is_trap := false
	if game_mode.traps_enabled and not Arrows.is_slowed:
		is_trap = _rng.randi_range(1, 7) == 1
	Arrows.animate_arrow(arrow, is_trap)


func _animate_start_arrow() -> void:
	Arrows.show_start_arrow()


func _slow_down_prompts() -> void:
	const FIRST_SPEED : float = 1.75
	const SECOND_SPEED : float = 2.25
	const FIRST_TICK := 0.50
	const SECOND_TICK := 0.33
	
	if Arrows.is_slowed:
		Arrows.set_prompt_speed(Arrows.current_prompt_speed / SECOND_SPEED)
		Arrows.slowdown_tick = SECOND_TICK
	else:
		Arrows.is_slowed = true
		Arrows.slowdown_tick = FIRST_TICK
		Arrows.set_prompt_speed(Arrows.target_prompt_speed / FIRST_SPEED)


func _remove_boxes(amount : int = 3) -> void:
	var total_removed : int = Box_Pool.remove_boxes(amount)
	if total_removed > 0:
		set_next_point(Vector3(0, -box_separation * total_removed, 0))
		GameState.total_boxes -= total_removed


func _calculate_prompt_speed() -> void:
	# Slow down the first few prompts at the start of game
	if _start_prompt_step < 3:
		_start_prompt_step += 1
		Arrows.set_target_prompt_speed(game_mode.base_prompt_speed * _current_starting_prompt_speed)
		_current_starting_prompt_speed += _start_prompt_speed_increase
	elif _start_prompt_step == 3:
		Arrows.set_target_prompt_speed(game_mode.base_prompt_speed)
		_start_prompt_step += 1
	
	if not game_mode.scale_speed:
		return
	if GameState.total_boxes > game_mode.score_step_ceiling:
		return
	
	if GameState.total_boxes >= _next_score_step:
		_previous_score_step = _next_score_step
		_next_score_step = _next_score_step + game_mode.score_step_amount
		var increase : float = game_mode.speed_scale_amount * (float(GameState.total_boxes) / float(game_mode.score_step_amount))
		Arrows.set_target_prompt_speed(game_mode.base_prompt_speed + increase)
	# If score has decreased to the previous step
	elif GameState.total_boxes < _previous_score_step and _previous_score_step > 0:
		_next_score_step = _previous_score_step
		_previous_score_step = _previous_score_step - game_mode.score_step_amount
		var decrease : float = game_mode.speed_scale_amount * (float(_previous_score_step) / float(game_mode.score_step_amount))
		Arrows.set_target_prompt_speed(game_mode.base_prompt_speed + decrease)




#<---Signal functions---> 


func _on_arrow_prompts_arrow_swiped(swipe_direction : int):
	if !GameState.is_game_started():
		start_game()
	
	GameState.total_boxes+= 1
	GameState.on_box_added()
	Box_Pool.place_box(_next_point, swipe_direction, str(GameState.total_boxes))
	
	set_next_point()
	_animate_random_arrow()


func _on_arrow_prompts_arrow_miss(was_trap : bool):
	if !GameState.is_game_started():
		return
	
	GameState.tries_remaining -= 1
	if was_trap:
		var banan : Banana = BananaSpawner.spawn_banana(_next_point)
		if is_instance_valid(banan):
			# Wait for banana to collide with boxes before continuing
			await Signal(banan.reached_target)
	_remove_boxes()
	FollowCam.shake()
	GameState.on_box_removed(was_trap)
	if GameState.tries_remaining == 0:
		end_game()
		return
	# Add brief pause before starting the next prompt
	# In order to prevent prompting immediately after a miss
	await get_tree().create_timer(0.15).timeout
	if !was_trap:
		_slow_down_prompts()
	_animate_random_arrow()


func _on_follow_camera_camera_reset():
	#await get_tree().create_timer(1).timeout
	begin_playing()


func _on_high_score() -> void:
	Box_Pool.animate_top_box()
