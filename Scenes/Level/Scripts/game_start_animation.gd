extends Node3D

var transparent_material := preload("res://Assets/Materials/m_transparent.tres")
var box_material := preload("res://Assets/Materials/box_material.tres")

@export var follow_cam : FollowCamera
@export var audio_player : AudioPlayer

@onready var Box1 : Node3D = $Box1
@onready var Box2 : Node3D = $Box2
@onready var Box1Mesh : MeshInstance3D = $Box1/Box1Mesh
@onready var Box2Mesh : MeshInstance3D = $Box2/Box2Mesh
@onready var ScoreLabel : Label3D = $Box1/ScoreLabel
@onready var SmokeParticle : GPUParticles3D = $SmokeParticle

func _ready():
	reset()

func _process(delta: float) -> void:
	ScoreLabel.text = str(GameState._high_score)
	

#<---Public functions--->

# Animation where both boxes fall
func play() -> void:
	visible = true
	var tween := get_tree().create_tween()
	await tween.tween_property(Box1, "position", Vector3(0,0,0), 0.38).finished
	
	SmokeParticle.emitting = true
	if is_instance_valid(follow_cam):
		follow_cam.shake()	
	if is_instance_valid(audio_player):
		audio_player.play_miss_sound()
		
	tween = get_tree().create_tween()
	await tween.tween_property(Box2, "position", Vector3(0,0.255,0), 0.27).finished
	if is_instance_valid(audio_player):
		audio_player.play_deep_sound()


# Animation where top box falls
func play_second() -> void:
	visible = true
	var tween := get_tree().create_tween()
	await tween.tween_property(Box2, "position", Vector3(0,0.255,0), 0.27).finished	
	
	if is_instance_valid(follow_cam):
		follow_cam.shake()
	if is_instance_valid(audio_player):
		audio_player.play_deep_sound()
	
	Box1Mesh.material_override = transparent_material
	Box1.position = Vector3(0,0,0)
	Box1.rotation_degrees = Vector3(0,0,0)


# Animation where only labels are shown
func play_third() -> void:
	visible = true
	Box1Mesh.material_override = transparent_material
	Box1.position = Vector3(0,0,0)
	Box1.rotation_degrees = Vector3(0,0,0)
	
	Box2Mesh.material_override = transparent_material
	Box2.position = Vector3(0,0.255,0)
	Box2.rotation_degrees = Vector3(0,0,0)


func reset() -> void:
	Box1.position.y += 5
	Box1.rotation_degrees = Vector3(0, -10,0)
	Box1Mesh.material_override = box_material
	
	Box2.position.y += 5
	Box2.rotation_degrees = Vector3(0, 5,0)
	Box2Mesh.material_override = box_material
	visible = false

