extends Node

@export var camera : FollowCamera
var _banana_asset := preload("res://Scenes/Props/banana.tscn")


var rng = RandomNumberGenerator.new()

#<---Public functions--->

func spawn_banana(target : Vector3) -> Banana:
	if !is_instance_valid(camera):
		print("Cannot spawn banana. Camera not set")
		return
	
	var rotation := Vector3(rng.randf_range(-180, 180), rng.randf_range(-180, 180), rng.randf_range(-180, 180))
	var position := Vector3(camera.global_position.x - 1, camera.global_position.y - 0.25, camera.global_position.z + rng.randf_range(-1.25, 1.25))
	var banana : Banana = _banana_asset.instantiate()
	banana.global_rotation_degrees = rotation
	banana.position = position
	banana.target = target
	add_child(banana)
	return banana
	
#<---Private functions--->



#<---Signal functions---> 



