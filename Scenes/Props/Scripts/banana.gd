class_name Banana
extends Node3D

signal reached_target

@export var target : Vector3


func _physics_process(delta: float) -> void:
	position = position.lerp(target, delta * 6)
	rotate_x(delta * 20)
	if position.distance_to(target) <= 0.4:
		reached_target.emit()
		queue_free()

