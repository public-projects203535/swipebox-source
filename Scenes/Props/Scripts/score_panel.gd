extends Node3D



func _ready():
	GameState.game_started.connect(Callable(_game_start))
	GameState.game_ended.connect(Callable(_game_end))
	
	GameState.game_started.connect(Callable(_update_score))
	GameState.box_added.connect(Callable(_update_score))
	GameState.box_removed.connect(Callable(_update_score).unbind(1))
	GameState.gamemode_changed.connect(Callable(_game_start).unbind(1))


#<---Public functions--->

func _reset() -> void:
	$AnimationPlayer.stop()
	$AnimationPlayer.play("RESET")

#<---Private functions--->

func _set_text(text : String) -> void:
	$ScoreLabel.text = text


func _update_score() -> void:
	_set_text("Score: %s" % GameState.total_boxes)
	

func _game_start() -> void:
	$AnimationPlayer.stop()
	$AnimationPlayer.play("RESET")	


func _game_end() -> void:
	if GameState.is_high_score:
		$AnimationPlayer.play("HighScore")

	

#<---Signal functions---> 



