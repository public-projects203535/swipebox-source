class_name Box
extends Node3D

const default_color := Color(1,1,1,1)

@onready var label : Label3D = $MeshBox/Label3D

var _tween : Tween
var rng = RandomNumberGenerator.new()

var _is_spawned := false

func _ready():
	rng.randomize()
	

func _process(delta):
	if _is_spawned:
		label.modulate = _get_text_color()

#<---Public functions--->

func move(pos : Vector3) -> void:
	_is_spawned = true
	rotation_degrees = Vector3(0, 180, 0)
	_tween = create_tween().set_parallel(true)
	_tween.tween_property(self, "global_position", pos, 0.2)
	_tween.tween_property(self, "rotation_degrees", Vector3(0,0,0), 0.2)


func set_text(text : String) -> void:
	label.text = text
	

func set_text_color(color : Color) -> void:
	label.modulate = color


func remove(end_pos : Vector3) -> void:
	_is_spawned = false
	_tween.kill()
	var rand = rng.randi_range(0, 1)
	var fall_direction : float = 1 if rand == 0 else -1
	_tween = create_tween().set_parallel(true)
	var rand_rot = rng.randf_range(500, 1000)
	_tween.tween_property(self, "rotation_degrees", Vector3(0,rand_rot,0), 1)
	var rand_dist = rng.randf_range(8, 15)
	await _tween.tween_property(self, "global_position", global_position + Vector3(0, 0, fall_direction * rand_dist), 1).finished
	global_position = end_pos
	set_text_color(default_color)


func freeze() -> void:
	_tween.kill()
	

func animate() -> void:
	$AnimationPlayer.play("Pulse")



#<---Private functions--->

func _get_text_color() -> Color:
	if GameState.tries_remaining == 0 or GameState.tries_remaining == 1:
		return Color(1,0,0)
	elif GameState.tries_remaining == 2:
		return Color(1, 1, 0)
	else:
		return default_color
		
		
