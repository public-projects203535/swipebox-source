class_name AudioPlayer
extends AudioStreamPlayer

var box_sound := preload("res://Assets/Sound/whoosh_swish_high_fast_04.wav")
var miss_sound := preload("res://Assets/Sound/punch_general_body_impact_01.wav")
var deep_sound := preload("res://Assets/Sound/punch_low_deep_impact_04.wav")

var trap_sounds := [preload("res://Assets/Sound/punch_grit_wet_impact_02.wav"), 
					preload("res://Assets/Sound/punch_grit_wet_impact_03.wav"), 
					preload("res://Assets/Sound/punch_grit_wet_impact_07.wav")]

func _ready():
	GameState.box_added.connect(Callable(play_box_sound))
	GameState.box_removed.connect(Callable(play_miss_sound))


#<---Public functions--->

func play_box_sound() -> void:
	if !GameState.sound_enabled:
		return
	if stream != box_sound:
		stream = box_sound
		volume_db = 0
	play()


func play_miss_sound(is_trap := false) -> void:
	if !GameState.sound_enabled:
		return
	if is_trap:
		play_trap_sound()
		return
	if stream != miss_sound:
		stream = miss_sound
		volume_db = -15
	play()
	

func play_trap_sound() -> void:
	if !GameState.sound_enabled:
		return
	var rand_index := randi_range(0, trap_sounds.size() - 1)
	stream = trap_sounds[rand_index]
	volume_db = -15
	play()


func play_deep_sound() -> void:
	if !GameState.sound_enabled:
		return
	if stream != deep_sound:
		stream = deep_sound
		volume_db = -10
	play()

