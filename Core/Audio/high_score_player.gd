extends AudioStreamPlayer

var miss_sound := preload("res://Assets/Sound/collect_item_02.wav")

func _ready():
	stream = miss_sound
	GameState.high_score.connect(Callable(play_highscore))

#<---Public functions--->

func play_highscore() -> void:
	if !GameState.sound_enabled:
		return
	play()

