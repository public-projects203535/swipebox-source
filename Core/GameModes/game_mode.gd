class_name GameMode
extends Resource
enum ModeName {SLOW, CASUAL, NORMAL, CHALLENGE, HARDCORE}
@export var mode_name : ModeName = ModeName.CASUAL
@export var traps_enabled := false
@export var scale_speed := false
@export var base_prompt_speed : float = 1.7
@export var speed_scale_amount : float = 0
# Score needed before increasing speed by the speed scale amount
@export var score_step_amount : int = 0
@export var score_step_ceiling : int = 0
@export var max_tries : int = 3
