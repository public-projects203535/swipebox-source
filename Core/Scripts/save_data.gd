extends Node

signal save_deleted

var loaded_data := {}


func _ready() -> void:
	_reset_data()


#<---Public functions--->

func save(mode : GameMode.ModeName, score : int) -> void:
	loaded_data[str(mode)] = score
	var save_game = FileAccess.open("user://savegame.save", FileAccess.WRITE)
	var json_string = JSON.stringify(loaded_data)
	save_game.store_line(json_string)
	#print("Save: ", loaded_data)


func load_game() -> void:
	if not FileAccess.file_exists("user://savegame.save"):
		print("No save data found")
		return
	var saved_game = FileAccess.open("user://savegame.save", FileAccess.READ)
	while saved_game.get_position() < saved_game.get_length():
		var json_string = saved_game.get_line()
		var json = JSON.new()
		var parse_result = json.parse(json_string)
		if not parse_result == OK:
			print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())
			continue
		loaded_data = json.get_data()


func delete_save() -> void:
	var dir := DirAccess.open("user://")
	if dir != null:
		dir.remove("user://savegame.save")
	_reset_data()
	save_deleted.emit()

	
#<---Private functions--->

func _reset_data() -> void:
	loaded_data = {"0" : 0, "1" : 0, "2" : 0, "3" : 0, "4" : 0}
