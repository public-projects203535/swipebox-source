extends Node

signal game_started
signal gamemode_changed(mode : GameMode)
signal box_added
signal box_removed(was_trap : bool)
signal game_ended
signal high_score
signal menu_entered
signal menu_exited

var modes := [
preload("res://Core/GameModes/GameMode_Slow.tres"),
preload("res://Core/GameModes/GameMode.tres"),
preload("res://Core/GameModes/GameMode_Normal.tres"),
preload("res://Core/GameModes/GameMode_Challenge.tres"),
preload("res://Core/GameModes/GameMode_Hardcore.tres")
]

var debug_auto_prompt := false

var _game_started := false

var total_boxes : int = 0
var tries_remaining : int = 3
var _high_score : int = 0

var current_mode : GameMode.ModeName = GameMode.ModeName.CASUAL
var is_high_score := false
var sound_enabled := true
var is_in_menu := false : set = set_is_in_menu

func _ready():
	SaveData.save_deleted.connect(Callable(save_deleted))
	SaveData.load_game()
	_high_score = SaveData.loaded_data[str(current_mode)]

#<---Public functions--->

func switch_mode(mode : GameMode.ModeName) -> void:
	current_mode = mode
	_high_score = SaveData.loaded_data[str(current_mode)]
	is_high_score = false
	gamemode_changed.emit(modes[mode])


func start_game(_tries_remaining : int = 3) -> void:
	_game_started = true
	is_high_score = false
	tries_remaining = _tries_remaining
	total_boxes = 0
	game_started.emit()
	

func end_game() -> void:
	_game_started = false		
	if total_boxes > _high_score:
		_high_score = total_boxes
	game_ended.emit()
	if is_high_score:
		SaveData.save(current_mode, _high_score)


func on_box_added() -> void:
	if (!is_high_score and total_boxes > _high_score) and _high_score > 0 :
		high_score.emit()
	is_high_score = total_boxes > _high_score
	
	box_added.emit()


func on_box_removed(was_trap : bool) -> void:
	is_high_score = total_boxes > _high_score
	box_removed.emit(was_trap)
	

func is_game_started() -> bool:
	return _game_started
	

func set_is_in_menu(in_menu) -> void:
	is_in_menu = in_menu
	if is_in_menu:
		menu_entered.emit()
	else:
		menu_exited.emit()
		


#<---Private functions--->



#<---Signal functions---> 

func save_deleted() -> void:
	is_high_score = false
	_high_score = 0



